<?php

namespace Drupal\agiledrop_test_module\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\agiledrop_test_module\Service\GetDateService;

/**
 * Provides a 'EventBlock' block.
 *
 * @Block(
 *  id = "event_block",
 *  admin_label = @Translation("Event block"),
 *  category = @Translation("Event blocks")
 * )
 */

class EventBlock extends BlockBase implements ContainerFactoryPluginInterface {
  /**
   * Drupal\agiledrop_test_module\Service\GetDateService definition.
   *
   * @var \Drupal\agiledrop_test_module\Service\GetDateService
   */
  protected $eventService;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, GetDateService $eventService) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->eventService = $eventService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, 
      $plugin_id, 
      $plugin_definition, 
      $container->get('agiledrop_test_module.get_event_date') // Get the service from the container.
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#markup' => $this->eventService->getEventMessage(new \DateTime()),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->t('Event status');
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0; // Disable caching.
  }
}