<?php

namespace Drupal\agiledrop_test_module\Service;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Class GetDateService.
 * Service to get the date of the event.
 */
class GetDateService {
  protected $entityTypeManager;

  /**
   * Constructs a new GetDateService object.
   * @param EntityTypeManagerInterface $entity_type_manager
   *  The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Get the date of the event. If there is no event date, return NULL.
   * @return \DateTime|null
   *  The date of the event.
   */
  private function getEventDate() {
    $currentNode = \Drupal::routeMatch()->getParameter('node');
    if ($currentNode && $currentNode->getType() == 'event') {
      $eventDate = $currentNode->get('field_date')->getValue();
      $date = $eventDate[0]['value'];
      return new \DateTime($date);
    }
    
    return NULL;
  }

  /**
   * Returns the message for the event. Depending on the date of the event.
   * Possible messages:
   * - No event date found. (if there is no event date on the node)
   * - X days left until event starts. (if the event is in the future)
   * - This event already passed. (if the event is in the past)
   * - This event is happening today. (if the event is today)
   * @param \DateTime $currentDate
   *  The current date.
   * @return string
   *  The message for the event.
   */
  public function getEventMessage(\DateTime $currentDate) {
    $date = $this->getEventDate();
    $status = '';
    if (!$date) {
      return 'No event date found.'; // Return early if there is no event date.
    }
    // Calculate the difference between the dates.
    $date->setTime(0, 0, 0);
    $currentDate->setTime(0, 0, 0);
    $days = $currentDate->diff($date)->format('%r%a'); // %r is for the sign of the difference. %a is for the absolute value of the difference.

    match (true) {
      $days > 0 => $status = $days . ' days left until event starts.',
      $days < 0 => $status = 'This event already passed.',
      default => $status = 'This event is happening today.',
    };

    return $status;
  }
}