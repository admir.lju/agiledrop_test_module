# Agiledrop test module

The Agile Test Module creates a new block that is used for Events. Depending on the event date field it displays 4 possible strings:
- No event date found.
- X days left until event starts
- This event already passed.
- This event is happening today.

## Table of contents

- Requirements
- Recommended modules
- Installation
- Maintainers

## Requirements

This module requires no modules outside of Drupal core.

## Installation 

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Maintainers

- [Admir Ljubijankić](mailto:admir.lju@gmail.com)